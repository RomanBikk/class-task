package CarPack;

import java.util.Date;

public class Car implements IMoveable {
    private String model;
    private double maxSpeed;
    private int manufactureYear;
    String color;

    @Override
    public void move() {
        System.out.println("Riding around");
    }
    public double turboSpeed(double maxSpeed, double extraSpeed) {
        return maxSpeed + extraSpeed;
    }

    private Car() {


    }
    public Car(String model, double maxSpeed, int manufactureYear, String color) {
        this.model = model;
        this.maxSpeed = maxSpeed;
        this.manufactureYear = manufactureYear;
        this.color = color;
    }
    public Car(String model, double maxSpeed, int manufactureYear) {
        this.model = model;
        this.maxSpeed = maxSpeed;
        this.manufactureYear = manufactureYear;
    }
    public Car(String model, String color) {
        this.model = model;
        this.color = color;
    }

    public String getModel() {
        return model;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }
    public int getManufactureYear() {
        return manufactureYear;
    }
    public String getColor() {
        return color;
    }
    public void setModel(String model) {
        this.model = model;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public void setManufactureYear(int manufactureYear) {
        this.manufactureYear = manufactureYear;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
