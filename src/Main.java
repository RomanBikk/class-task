import CarPack.Car;

public class Main {
    public static void main(String[] args) {
    Car jeep = new Car("Jeep Cherokee", 145.6, 1992, "golden brown" );
    jeep.move();
    System.out.println(jeep.getMaxSpeed() + " - max speed");
    System.out.println(jeep.turboSpeed(jeep.getMaxSpeed(), 27.6)+ " - turbo speed");
    }
}
